// article模块接口列表示例
import { sq } from './base'
import axios from '../http' // 导入http中创建的axios实例
import qs from 'qs' // 根据需求是否导入qs模块

const article = {
    // 新闻列表
    articleList() {
        return axios.get(`${sq}/topics`)
    },
    // 新闻详情,演示
    articleDetail(id, params) {
        return axios.get(`${sq}/topic/${id}`, {
            params: params,
        })
    },
    // post提交、qs示例
    login(params) {
        return axios.post(`${sq}/accesstoken`, qs.stringify(params))
    },
}

export default article
