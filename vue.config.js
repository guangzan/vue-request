module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
    lintOnSave: false,
    devServer: {
        open: false,
        host: 'localhost',
        port: '8080',
        https: false,
        hotOnly: true,
        compress: true,
    },
}
